\input texinfo.tex    @c -*-texinfo-*-
@c %**start of header
@setfilename egg.info
@settitle EEG (Draft) Manual
@documentencoding utf-8
@paragraphindent none
@c %**end of header

@dircategory Emacs
@direntry
* Egg: (egg).        Emacs Got Git
@end direntry

@copying
Copyright @copyright{} 2009, 2010, 2011 Bogolisk <bogolisk@@gmail.com>.

@quotation
Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.2 or
any later version published by the Free Software Foundation; with no
Invariant Sections, with no Front-Cover Texts, and with no Back-Cover
Texts.
@end quotation
@end copying

@node Top, Rant, (dir), (dir)
@top Egg Manual

@sc{Egg} is an attempt to provide a seamless integration of @sc{Git}
within @sc{Emacs}.  @sc{Egg} is currently tested with @sc{Git} 1.7.12
and @sc{Emacs} 23. It may work with other versions of @sc{Emacs} and
@sc{Git} but the @sc{Egg}'s developer does not have time to investigate
and fix bugs that only appear in versions or platforms that he doesn't
use. Patches to fix bugs in other emacsen or volunteers to maintain
compatibility however are welcome.

@menu
* Rant::                        
* Acknowledgements::            
* Overview::                    
* File::                        
* Status::                      
* Log::                         
* Customization::               
* Frequently Asked Questions::  

@detailmenu
 --- The Detailed Node Listing ---

Overview

* Buffers::                     
* Sections::                    
* Diff Sections::               

What's in a File

* Minor Mode::                  
* Blame::                       

Playing with your HEAD

* Unstaged::                    
* Staged::                      
* Untracked::                   
* Status Buffer Commands::      

Messing with History

* Commit Line::                 
* Log Buffer Ref::              

@end detailmenu
@end menu

@node Rant, Acknowledgements, Top, Top
@unnumbered Rant

@sc{Egg} wasn't designed to replace the @sc{Git} excellent official
porcelain layer, especially when used with the @t{bash_completion}
package. In the @sc{Egg}'s developer's opinion: it's pointless to write
a @t{git-mode} in emacs to let user type @kbd{M-x git-reset} when he/she
can do exactly the same thing in @sc{Bash}. An emacs interface to
@sc{Git} should provide the conveniences that would make it easier to
accomplish the same task than the @sc{Git}'s porcelain. @sc{Egg}, thus,
was designed to perform the same goal as @sc{Git Aliases}. Make it more
convenient to accomplish common git operations.

One example:

The in @sc{Egg's} status buffer, when the user @emph{unstages} a file,
@sc{Egg} would invoke different git operations depend on the
origin/status of the file: it was an existing file, it was new file or
it was a file with merge conflicts which were just resolved in the
index.

Again, @sc{Egg} was design to help you accomplish many git common
operations in very convenient @sc{Emacs} way.

As with @sc{Git Aliases}, @sc{Egg} won't save you from learning
@sc{Git}. Instead it expects its user to be familiar with @sc{Git}. This
manual tries to document all @sc{Egg} functionalities and explain the
design of some of the commands.

@node Acknowledgements, Overview, Rant, Top
@unnumbered Acknowledgements

In the beginning, @sc{Egg} was a fork of @sc{Marius Vollmer}'s excellent
@sc{Magit}. While @sc{Egg} was later completely rewritten, it still
keeps the genius @emph{staging/unstaging} metaphor in the @emph{status
buffer}.

@sc{ByPlayer} gracefully picked up the maintainership when Bogolisk went
hiatus for several... years!!! He's the current maintainer of @sc{Egg}
and his repo is at: @file{https://github.com/byplayer/egg}

@node Overview, File, Acknowledgements, Top
@unnumbered Overview

Using @sc{Egg} is simple:
@itemize
@item
download @file{egg.el} and put in your @sc{Emacs}'s @code{loadpath}.
@item
add @code{(require 'egg)} in your @file{.emacs}
@item
very important: disable @sc{Vc}'s git backend.
Do @kbd{M-x customize-option RET vc-handled-backends}.
Then @key{DEL} the Git option.
@item
open a file in a git repo from within emacs. Et voila!
@end itemize

@menu
* Buffers::                     
* Sections::                    
* Diff Sections::               
@end menu

@node Buffers, Sections, Overview, Overview
@unnumberedsec The Many Faces of Egg
When the user visits files from a git repository with, @sc{Egg} creates
(on demand) several @emph{egg special buffers}. Some of them are:

(in the following list, @t{REPO} is name of the git repo and @t{GIT-DIR}
is the path of the git directory.)

@itemize
@item  @t{*REPO-status@@GIT-DIR*}
 the status buffer. it's intended to manipulate the index such as
 resolving merge.
@item @t{*REPO-log@@GIT-DIR*}
 the log buffer. This is probably the most feature-full egg special
 buffer. It's intended: to inspect and manipulate the repo history such
 as create/destroy refs; to push/fetch branches to/from remote repo;
 pickaxing; cherry-picking; reverting; merging; interactive-rebasing...

 It has too cousins: @t{*REPO-stash@@GIT-DIR*} and
 @t{*REPO-reflog@@GIT-DIR*}. These are used to view and manipulate the
 stashes and reflogs.

@item @t{*REPO-commit@@GIT-DIR*}
 the commit buffer. It's used to composed commit message. It's also has a
 cousin, @t{*REPO-tag@@GIT-DIR*}, which is used to compose the message
 for annotated tags.

@item @t{*REPO-diff@@GIT-DIR*}
 the diff buffer. It's used to compare files vs index or other
 revisions.
@end itemize

For example, if your standard repo was @file{/path/to/foo/} then the status
buffer would be: @t{*foo-status@/path/to/foo/.git*}.

@sc{Egg}'s special buffers have some common key bindings:
@itemize
@anchor{egg-buffer-mode-map}
@item @kbd{q} (@code{egg-quit-buffer})
 leaves and burries the buffer.
@item @kbd{g} (@code{egg-buffer-cmd-refresh})
 redisplay the contents of the buffer.
@item @kbd{n} (@code{egg-buffer-cmd-navigate-next})
moves cursor to the next interesting thing.
@item @kbd{p} (@code{egg-buffer-cmd-navigate-prev})
moves cursor to the previous interesting thing.
@end itemize


@node Sections, Diff Sections, Buffers, Overview
@unnumberedsec Divide and Conquer

@sc{Egg} speciall buffers usually contains more than one
@emph{sections}.These sections can be hidden and shown individually.
When a section is hidden, only its first line is shown and all its
children are completely invisible. The following keys are bound to
section-specific commands when the cursor is on a section. Those keys
are bound in a local @t{keymap} thus will only invoke the @sc{Egg}'s
command when the cursor is in a section.

@anchor{egg-section-map}
@itemize
@item @kbd{h} or @key{mouse-2} (@code{egg-section-cmd-toggle-hide-show})
toggles the visibility of a section.
@item @kbd{H} (@code{egg-section-cmd-toggle-hide-show-children})
toggles the visibility of the @emph{subsections} of the section.
@item @kbd{n} (@code{egg-buffer-cmd-navigate-next})
moves point to the next section
@item @kbd{p} (@code{egg-buffer-cmd-navigate-prev})
moves point to the previous section
@end itemize

@node Diff Sections,  , Sections, Overview
@unnumberedsec Vive la Difference
Many @sc{Egg}'s special buffers contain @emph{diff sections}. Each diff
section has a header with information about the file being compared and
revision information followed by a sequence of hunks. A diff section
inherits all local keybindings of a section
@xref{egg-section-map}. Plus, when the cursor is in the header part of
the diff section, the following keys are bound to @sc{Egg}'s commands:

@itemize
@anchor{egg-diff-section-map}
@item @kbd{RET} (@code{egg-diff-section-cmd-visit-file-other-window})
 visits the file (refered to by the diff header) in the other window.
@item @kbd{f} (@code{egg-diff-section-cmd-visit-file})
 visits the file (refered to by the diff header) in the current window.
@item @kbd{=} (@code{egg-diff-section-cmd-ediff})
 launches ediff to the compare file (refered to by the diff header).  the
 two operands commpared in ediff would be same as described in the diff
 header.
@end itemize

When the cursor is in a hunk, in addition to the keys bound for a
section @xref{egg-section-map}, the following keys are defined:

@itemize
@anchor{egg-hunk-section-map}
@item @kbd{RET} (@code{egg-hunk-section-cmd-visit-file-other-window})
 visits the file (refered to by the diff header) in the other window.
 The command also moves point to the location described in the hunk.
@item @kbd{f} (@code{egg-hunk-section-cmd-visit-file})
 visits the file (refered to by the diff header) in the current window.
 The command also moves point to the location described in the hunk.
@item @kbd{=} (@code{egg-diff-section-cmd-ediff})
@anchor{egg-diff-section-cmd-ediff}
 launches ediff to the compare file (refered to by the diff header).  the
 two operands commpared in ediff would be same as described in the diff
 header.
@end itemize

@node File, Status, Overview, Top
@unnumbered What's in a File
When you're editing a file from a git repository, @sc{Egg} provides a
number of commands in the @code{egg-minor-mode} to perform
@sc{git}-related operations.  @sc{Egg} also provide a @sc{blame-mode} to
view the current file with annotations.

@menu
* Minor Mode::                  
* Blame::                       
@end menu

@node Minor Mode, Blame, File, File
@unnumberedsec No Big Deal
When visiting a file in a @sc{Git} repository, @sc{Egg}'s minor-mode
provides the following commands. They are bound to the prefix @kbd{C-x
v}. Most of them also behave differently with prefixes (@kbd{C-u} and
@kbd{C-u C-u})

@itemize
@item @kbd{a} (@code{egg-file-toggle-blame-mode})
 toggles the @emph{view annotations} or @emph{blame mode}. If the buffer
 was modified, the command will prompt the user to offer to save the
 current buffer first.  With @kbd{C-u} prefix, the command will save the
 current buffer without prompting. @xref{Blame, Annotations or Blame},
 for details on @emph{blame-mode}.
@item @kbd{b} (@code{egg-start-new-branch}) @anchor{egg-start-new-branch}
 starts a new branch (from the current HEAD.) With the @kbd{C-u} prefix, a branch
 will be created even if it would replace an existing branch.
@item @kbd{d} or @kbd{s} (@code{egg-status})
 shows (without selecting) the status buffer. With @kbd{C-u} prefix, the
 status buffer will be selected. @xref{Status}, for more details.
@item @kbd{c} or @kbd{w} (@code{egg-commit-log-edit})
 starts composing a message to commit. With @kbd{C-u} prefix, the
 composed message will be used for amending the latest commit and the
 existing message will be pre-inserted into the buffer for editing.
 With @kbd{C-u C-u} prefix, the index will be used to immediately (no
 editing) amend the latest commit, re-using its existing message.
 cf:egg-commit-log-edit
@item @kbd{e} (@code{egg-file-ediff}) @anchor{egg-file-ediff}
 prompts for another revision and compare the contents of the current
 file in the work-tree using @t{ediff}. With @kbd{C-u} prefix, prompt
 for @emph{two} different revisions and to compare their's contents of
 the file using @t{ediff}.
@item @kbd{g} (@code{egg-grep})
 runs @t{git-grep} in a @t{grep-mode} buffer. With @kbd{C-u} prefix, use
 the string at point to as the default search term. With @kbd{C-u C-u}
 prefix, prompt for a revision (instead of the current checked-out
 contents in the work-tree) to perform the search. cf:egg-grep
@item @kbd{i} (@code{egg-file-stage-current-file}) @anchor{egg-file-stage-current-file}
 adds the current file's contents into the index.
@item @kbd{l} (@code{egg-log})
 launchs the log buffer to view the current @t{HEAD}'s history. With
 @kbd{C-u} prefix, show the history of all branches. @xref{Log}.
@item @kbd{L} (@code{egg-reflog})
 launch the reflog buffer to view @t{HEAD}'s reflog. With @kbd{C-u}
 prefix, prompt for a branch name then show its reflog. cf:egg-reflog
@item @kbd{h} (@code{egg-file-log})
 launch the filelog buffer to view current file's history in the current
 branch. With @kbd{C-u} prefix, show the current file's history in all
 branches. cf:egg-file-log
@item @kbd{o} (@code{egg-file-checkout-other-version}) @anchor{egg-file-checkout-other-version}
 checkout the current file's contents from a different revision. With @kbd{C-u}
 prefix, will not prompt for confirmation if the file has uncommitted changes.
@item @kbd{S} (@code{egg-statsh})
 launch the stash buffer to view repo's stashes. cf:egg-stash
@item @kbd{u} (@code{egg-file-cancel-modifications}) @anchor{egg-file-cancel-modifications}
 throw away all unstaged modifications of the current
 file. I.e. checkout the current file contents from the index. With
 @kbd{C-u} prefix, will not prompt for confirmation if the file has
 unstaged changes.
@item @kbd{v} (@code{egg-next-action})
 guess and perform the next @emph{logical} step of the @emph{workflow}.
 With @kbd{C-u} prefix, prompt for confirmation before starting the
 @emph{next} action. cf:egg-next-action
@item @kbd{/} (@code{egg-file-log-pickaxe})
 search the current file's history to the string under the cursor.
 cf:egg-file-log-pickaxe
@item @kbd{=} (@code{egg-file-diff}) @anchor{egg-file-diff}
 open the @emph{diff buffer} to compare the current file's contents in
 the work-tree vs the index. With @kbd{C-u} prefix, will prompt for a
 revision as base of the comparison instead of the index.
@item @kbd{~} (@code{egg-file-version-other-window}) @anchor{egg-file-version-other-window}
 show the staged (from the index) contents of the current buffer With
 @kbd{C-u} prefix, will prompt for a revision and show the file's
 contents from that revision.
@end itemize 

@node Blame,  , Minor Mode, File
@unnumberedsec Pointing Finger
The command @code{egg-file-toggle-blame-mode} will bring the current
file in and out of @emph{annotation-mode} a.k.a @emph{blame-mode}.

In annotation mode, the buffer is read-only. Block of lines is preceded
by an annotation about the last commit that changed that part of the
file.  The annotatin is a display-only (its text doesn't really exist in
the buffer) line similar to this:

@code{453f3f13   Bogolisk                  ignore and README}

The leading hexadecimal number is the sha1 of the commit. The following
part is the name of the committor and the subject of the commit. The
following keys are bound to @sc{Egg} commands in blame-mode.

@itemize
@item @kbd{l} or @kbd{RET} (@code{egg-blame-locate-commit})
 locates the @emph{blamed} commit in the the repo's history
 (@code{egg-log}, the command will open the repo history
 and move the cursor to the @emph{blamed} commit. With @kbd{C-u} prefix,
 the history of all branches will be shown (instead of just the current
 branch.) @xref{Log}.
@item @kbd{q} (@code{egg-file-toggle-blame-mode})
 leaves annotations mode and back to normal editing mode.
@item @kbd{n} (@code{egg-buffer-cmd-navigate-next})
 jump to the next annotation.
@item @kbd{p} (@code{egg-buffer-cmd-navigate-prev})
 jump to the previous annotation.
@end itemize

@node Status, Log, File, Top
@unnumbered Playing with your HEAD
The command @code{egg-status} open the status buffer. The top of the
status buffer shows the current HEAD (whether it's @emph{symbolic} or
@emph{detached}).  It also shows the current sha1 of HEAD as well as the
git directory.

Below the top part is the @sc{Help} section, described the common key
bindings of in the status buffer. This is @sc{Help} section's visibility
can be toggled as described in @ref{Sections}.

Following the Help section are the @sc{Unstaged Changes}, @sc{Staged
Changes} and @sc{Untracked Files} sections. Each of them inherits the
local keybindings as described in @ref{Sections}.

@menu
* Unstaged::                    
* Staged::                      
* Untracked::                   
* Status Buffer Commands::      
@end menu

@node Unstaged, Staged, Status, Status
@unnumberedsec The Candidates
If there were differences between the index and the work-dir, the
@sc{Unstaged Files} section would contains a sequence of diff sections
(@xref{Diff Sections}.) In addition to the key bindings described in
@ref{egg-section-map,,Section Key Bindings} and
@ref{egg-diff-section-map,,Diff Section Key Bindings}, the following
key bindings are defined when the cursor is in a diff header in the
@sc{Unstaged Files} section:

@itemize
@anchor{egg-unstaged-diff-section-map}
@item @kbd{=} (@code{egg-unstaged-section-cmd-ediff})
 launches ediff to compare the file's contents in the work-dir vs the index.
@item @kbd{s} (@code{egg-diff-section-cmd-stage})
 adds the file's contents to the index.
@item @kbd{u} (@code{egg-diff-section-cmd-undo})
 throws away unstaged local modifications. The command basically
 checkouts the file's contents from the index.
@item @kbd{x} (@code{egg-diff-section-revert-to-head})
 throw away all unstaged and staged changes. The command basically
 checkouts the file's contents from HEAD.
@item @key{mouse-2}
 shows a menu for the bound commands on a unstaged diff section.
@end itemize

If there were unmerged conflicts in the work-dir, then the
@sc{Unstaged Files} section would instead contain a sequence of
combined diff sections describing the merge conflicts. In this case,
the local key bindings are exactly the same as described in
@ref{egg-unstaged-diff-section-map,,Unstaged Diff Section Key
Bindings}, with one difference. The key @kbd{=} would launch a 3-way
ediff section between @emph{ours}, @emph{theirs} and the work-dir's
contents of the file to resolve the conflicts.

@node Staged, Untracked, Unstaged, Status
@unnumberedsec The Chosen Ones
If there were differences between the index and HEAD, the @sc{Staged
Files} section would contains a sequence of diff sections (@xref{Diff
Sections}.) In addition to the key bindings described in
@ref{egg-section-map,,Section Key Bindings} and
@ref{egg-diff-section-map,,Diff Section Key Bindings}, the following
key bindings are defined when the cursor is in a diff header in the
@sc{Staged Files} section:

@itemize
@anchor{egg-staged-diff-section-map}
@item @kbd{=} (@code{egg-staged-section-cmd-ediff3})
 launches 3-way ediff to compare the file's contents in the work-dir vs
 the index vs HEAD.
@item @kbd{s} (@code{egg-diff-section-cmd-unstage})
 remove the staged modifications of the current file in the index.
This command basically reset the slot of current file in the index. If
the file was a new file, the command will remove its slot in the index.
@item @kbd{x} (@code{egg-diff-section-revert-to-head})
 throw away all unstaged and staged changes. The command basically
 checkouts the file's contents from HEAD.
@item @key{mouse-2}
 shows a menu for the bound commands on a staged diff section.
@end itemize

If there were resolved merge conflicts in the index, the command
@code{egg-diff-section-cmd-unstage}, bound to the key @kbd{s}, will
restore the unresoved state of the file.

@node Untracked, Status Buffer Commands, Staged, Status
@unnumberedsec The Outsiders
The @sc{Untracked Files} section of the status buffer lists the
untracked files in the work-dir. The user can visit them, add them to
the index or ignore them using the commands listed below.

@itemize
@item @key{RET} (@code{egg-find-file-at-point})
visits the file under the cursor.
@item @key{DEL} (@code{egg-ignore-pattern-from-string-at-point})
add a pattern to the @file{.gitignore} file based on the file name
under the cursor.
@item @kbd{s} or @kbd{i} (@code{egg-status-buffer-stage-untracked-file})
add the file under the cursor or, if the region was active, the files
in the region into the index. with @kbd{C-u} prefix, only add the
entries for the new file(s) in the index but do not add their
contents.
@end itemize

@node Status Buffer Commands,  , Untracked, Status
@unnumberedsec More Toys to play with

Other than the commands described in @ref{egg-buffer-mode-map} and the
commands bound locally to the above sections (Unstaged, Staged and
Untracked), the status buffer also bind the following commands:

@itemize
@anchor{egg-status-buffer-mode-map}
@item @kbd{c} (@code{egg-commit-log-edit})
starts composing a message to commit. With @kbd{C-u} prefix, the
composed message will be used for amending the latest commit and the
existing message will be pre-inserted into the buffer for editing.
With @kbd{C-u C-u} prefix, the index will be used to immediately (no
editing) amend the latest commit, re-using its existing message.
cf:egg-commit-log-edit
@item @kbd{o} (@code{egg-status-buffer-checkout-ref})
prompts for a ref name and check it out.
@item @kbd{l} (@code{egg-log})
launches the log buffer to view the current @t{HEAD}'s history. With
@kbd{C-u} prefix, show the history of all branches. @xref{Log}.
@item @kbd{w} (@code{egg-status-buffer-stash-wip})
stash the index and the work-dir for later. The command will prompt
for a short description of the to-be-stashed work-in-progress. With
@kbd{C-u} prefix, the command will ask for confirmation whether
untracked files are to be included in the stash.
cf:egg-status-buffer-stash-wip
@item @kbd{W} (@code{egg-stash})
shows the stack of stashed work-in-progress entries. cf:egg-stash
@item @kbd{L} (@code{egg-reflog})
launches the reflog buffer to view @t{HEAD}'s reflog. With @kbd{C-u}
prefix, prompt for a branch name then show its reflog. cf:egg-reflog
@item @kbd{S} (@code{egg-stage-all-files})
add all tracked files's contents to the index
@item @kbd{U} (@code{egg-unstage-all-files})
remove all staged modifications from the index. This commands basically reset
the index back to HEAD.
@item @kbd{X} (@code{egg-status-buffer-undo-wdir})
throws away all unstaged modifications. With @kbd{C-u} prefix, the
commands throws away all uncommited modifications (staged and
unstaged.) This commands basically reset the index and the work-dir
back to HEAD.
@item @kbd{d} (@code{egg-diff-ref})
prompt for a revision and open the @emph{diff buffer} to compare the
work-tree's contents in the work-tree vs that revision. This commands
compares the whole work-tree while @ref{egg-file-diff} compares a single
file.
@end itemize

@node Log, Customization, Status, Top
@unnumbered Messing with History
The command @code{egg-log} open the log buffer showing history of the
current branch. With prefix argument @kbd{C-u}, it wouuld show the
history of all branches. This log buffer is the place where the user
manipulate the repo's history. Merge, rebase, push and fetch
operations are initiated from this buffer. Adding, removing and
updating local refs are also done here.

As with the status buffer, the log buffer also show the current branch
or the detached HEAD, HEAD's sha1 and the git directory. The
@emph{history scope} line shows the current scope of the history
listing. The follow block is the help section. It lists the commands
bound in @code{egg-log-buffer-mode-map} and the keys described in
@ref{egg-buffer-mode-map}:

@itemize
@anchor{egg-log-bufer-mode-map}
@item @kbd{s} (@code{egg-status})
launches the status buffer. @xref{Status}.
@item @kbd{L} (@code{egg-reflog})
launches the reflog buffer. cf:egg-reflog.
@item @kbd{/} (@code{egg-search-changes})
searches (pickaxe) history for commit introducing or removing a string.
the command open the @emph{query-commit buffer}. cf:egg-query:commit
@end itemize

@menu
* Commit Line::                 
* Log Buffer Ref::              
@end menu

@node Commit Line, Log Buffer Ref, Log, Log
@unnumberedsec Show Your Commitments
The history is shown in the buffer as a list of commit. On the left-most
side is the DAG in drawn using ascii-art. Next to the DAG is line of
dashes, followed by the sha1 then the subject of the commit. On the
dashed line, the refs are displayed if they point to the commit.  At
first the details (message and diff) are not retrieved. You can load the
details of a commit using the key @key{SPC}. The retrieved details of
the commit are display immediately under the commit line. The commit's
details can be hidden using the @kbd{h} key. When the commit's details
are retrieved but hidden, Emacs add @t{...} at the end of the commit
line.

When the details of the commit was retrieved and the cursor is
positioned on the @emph{diff} part of the commit, the local key
bindings is as described in @ref{egg-section-map} plus the following
keys:

@itemize
@anchor{egg-log-diff-map}
@anchor{egg-log-hunk-map}
@item @key{RET} (@code{egg-log-diff-cmd-visit-file-other-window} or
 @code{egg-log-hunk-cmd-visit-file-other-window}) If commit was at
HEAD, then visits the file (refered to by the diff header) in the
other window. Otherwise, retrieve the contents of the file from that
commit into a buffer (in the other window) and select it. With
@kbd{C-u} prefix, the command just visit the file's contents from
HEAD. If the cursor was inside a hunk, then the command will locate
the equivalent line in the buffer.
@item @kbd{f} (@code{egg-log-diff-cmd-visit-file})
behave exactly like
@code{egg-log-diff-cmd-visit-file-other-window}. If the cursor was
inside a hunk, then it behaves exactly as
@code{egg-log-hunk-cmd-visit-file-other-window}.  but use the current
window instead.
@item @kbd{=} (@code{egg-diff-section-cmd-ediff})
@xref{egg-diff-section-cmd-ediff}.
@end itemize

There are many keys bound locally when the cursor is on a commit
line. As already mentioned, all the keys described in
@ref{egg-section-map}, the other bound keys are:

@itemize
@anchor{egg-log-commit-map}
@item @key{SPC} (@code{egg-log-buffer-insert-commit})
retrieves and displays the details of a commit. The details include the
body of the commit message as well as the delta vs the previous commit.
@item @kbd{B} (@code{egg-log-buffer-create-new-branch})
creates a new branch starting from this commit. With prefix @code{C-u},
the command would force the creation of the branch even if there was
already an existing branch with the same name.
@item @kbd{b} (@code{egg-log-buffer-start-new-branch})
creates a new branch starting from this commit and check it out. With
prefix @code{C-u}, the command would force the creation of the branch
even if there was already an existing branch with the same name.
@item @kbd{o} (@code{egg-log-buffer-checkout-commit})
detachs HEAD and checkout the commit.
@item @kbd{t} (@code{egg-log-buffer-tag-commit})
creates a lightweight tag pointing to the commit. With @kbd{C-u} prefix,
the command would force the creation of the tag even if an existing tag
with the same already existed.
@item @kbd{T} (@code{egg-log-buffer-atag-commit})
@anchor{egg-log-buffer-atag-commit}
open the @emph{tag:msg buffer} for the user to start composing the
message of a new annotated tag pointing to the commit. With @kbd{C-u}
prefix, the to-be-created annotated tag would be gpg-signed. In this
case, the user will prompted for the gpg key uid to sign the message
when the editit was done. However, with @kbd{C-u C-u} prefix, Egg would
simply use the default gpg key. cf:tag:msg-buffer
@item @kbd{a} (@code{egg-log-buffer-attach-head})
@anchor{egg-log-buffer-attach-head} move HEAD and the current branch (if
it was attached) to the commit. The index will be updated but local
modifications are preserved. If the files locally modified are not
identical in the old commit and the new commit, Git will abort the
command. This basically the front-end for @code{git reset --keep}. With
@kbd{C-u} prefix, local modifications will be all discarded. The index
and the work tree will both be updated. This is basically the front-end
for @code{git reset --hard}. With @kbd{C-u C-u}, Egg will prompt the
user for the reset modes: keep, hard, soft, mixed and merged.
@item @kbd{m} (@code{egg-log-buffer-merge})
@anchor{egg-log-buffer-merge} merges the branch starting at the commit
to HEAD.  With @kbd{C-u} prefix, performs the merge but pretends the
merge failed and do not autocommit, to give the user a chance to inspect
and further tweak the merge result before committing. With @kbd{C-u C-u}
prefix, prompts the user for merge modes: normal, no-commit, squash and
ff-only.
@item @kbd{c} (@code{egg-log-buffer-pick-1cherry})
@anchor{egg-log-buffer-pick-1cherry} apply the commit onto the current
branch or HEAD. With @kbd{C-u} prefix, apply the commit but do not
autocommit. Instead, open the commit buffer to let the user re-compose
the message before commiting. cf:egg-commit-log-edit
@item @kbd{r} (@code{egg-log-buffer-rebase})
@anchor{egg-log-buffer-rebase} rebase the current branch while using the
commit as upstream. With @kbd{C-u} prefix, prompt the user for the
upstream revision. Then, rebase the current branch from the revision
immediately @emph{after} upstream @emph{onto} the commit.
@item @kbd{+}
mark the commit @emph{to be picked} for the upcomming interactive rebase
operation.
@item @kbd{.}
mark the commit @emph{to be squashed} for the upcomming interactive rebase
operation.
@item @kbd{~}
mark the commit @emph{to be editted} for the upcomming interactive rebase
operation.
@item @kbd{*} 
mark the commit as @emph{selected} for various operations.
@item @kbd{-} or @key{DEL}
unmark the commit.
@item @kbd{R} (@code{egg-log-buffer-rebase-interactive})
@anchor{egg-log-buffer-rebase-interactive} rebase the current branch
while using the commit as upstream. With @kbd{C-u} prefix, prompt the
user for the upstream revision. Then, rebase the current branch from the
revision immediately @emph{after} upstream @emph{onto} the commit.
@item @kbd{=} (@code{egg-log-buffer-diff-revs})
open the @emph{diff} buffer and compare the commit at point to the
@emph{marked} commit. If no commit was marked with *, compare the
commit to HEAD. This commands compares the whole work-tree while
@ref{egg-file-diff} compares a single file. cf:diff-buffer
@item @kbd{u} (@code{egg-log-buffer-push-to-local})
push the commit at point onto the either the @emph{marked} commit or
the current branch or the detached HEAD. With @kbd{C-u} prefix, the
command will prompt for the ref on which to push. With @kbd{C-u C-u}
prefix, the command will do non-fast-forward move. This command
usually uses @code{git push} underneath. However, if the push target
is the current branch, then the underlying git command might be
@code{git merge} or @code{git reset}.
@item @key{mouse-2}
show a context menu for the commit.
@end itemize


@node Log Buffer Ref,  , Commit Line, Log
@unnumberedsec For Your Reference

When the cursor is not just on the commit line but actually on a ref
name. The following keys are also locally bound:

@itemize
@anchor{egg-log-ref-map}
@item @kbd{L} (@code{egg-log-buffer-reflog-ref})
open the @emph{reflog} buffer but limit its scope to just the ref
under the cursor. cf:reflog.
@item @kbd{x} (@code{egg-log-buffer-rm-ref})
remove the ref under the cursor.
@anchor{egg-log-local-ref-map}
@item @kbd{d} (@code{egg-log-buffer-push-head-to-local})
push the current branch onto the ref under the cursor. With @kbd{C-u}
prefix, the command will do non-fast-forward move.
@item @kbd{U} (@code{egg-log-buffer-push-to-remote})
upload the local ref under the cursor to a remote repo. With @kbd{C-u}
prefix, the command will do non-fast-forward move. If the cursor was
@item @kbd{D} (@code{egg-log-buffer-fetch-remote-ref})
download the remote ref from its repo.
@anchor{egg-log-remote-ref-map}
@item @kbd{U} (@code{egg-log-buffer-push})
if the cursor is on the name of a remote repo (instead of the name of
the ref), ask the user which ref to upload or the command should
upload all refs to the remote repo.
@item @kbd{D} (@code{egg-log-buffer-fetch})
if the cursor is on the name of a remote repo (instead of the name of
the ref), ask the user which ref to download or the command should
download all refs from the remote repo.
@item @key{mouse-2}
shows the context menu for the ref under the cursor.
@end itemize


@node Customization, Frequently Asked Questions, Log, Top
@unnumbered Customization

The following variables can be used to adapt Egg to your workflow:

@kbd{M-x customize-group egg}

@node Frequently Asked Questions,  , Customization, Top
@unnumbered Just a FAQ Ma'am


@bye
